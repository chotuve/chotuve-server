const RequestManager = require("../Managers/ExternalManagers/RequestsManager")

const server_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTQ4NTM0MzIsIm5iZiI6MTU5NDg1MzQz" +
    "MiwianRpIjoiZWMwYWIxZWMtNmIxNS00NGNjLWE0ZTItNWI4ZWE1OTZjOTZkIiwiaWRlbnRpdHkiOiJodHRwczovL2Nob" +
    "3R1dmUtYXBwbGljYXRpb24tc2VydmVyLmhlcm9rdWFwcC5jb20vIiwidHlwZSI6InJlZnJl" +
    "c2giLCJ1c2VyX2NsYWltcyI6eyJzZXJ2ZXIiOnRydWV9fQ.klLra18fVSmrwKhENMGiLskZ5z2a8pRLEymBDZmVwnw";
const AUTHORIZE_URL = "https://chotuve-auth-server-g5-dev.herokuapp.com/authorize";

/**
 * @swagger
 *  securityDefinitions:
 *      Bearer:
 *          type: oauth2
 *          description: Se utilizan los tokens short-lived y refresh para autorizar el acceso a los recursos.
 *          authorizationUrl: https://chotuve-auth-server-g5-dev.herokuapp.com/authorize
 *          refreshUrl: https://chotuve-auth-server-g5-dev.herokuapp.com/authenticate
 *          flow: Short-lived and refresh token
 *          scopes:
 *              user: Grants user access to certain resources.
 *              admin: Grants admin access to all resources.
 * security:
 *  - Bearer: [user]
 */

async function authorize_admin(sl_token){
    const header = {
        'Content-Type': 'application/json',
        'Authorization': sl_token,
        'App-Server-Api-Key': server_token
    }
    const result = await RequestManager.generatePostWithHeaders(AUTHORIZE_URL, {admin:true}, header);
    return (result.status === 200);
}

module.exports = async (req, res, next) => {
    try {
        const sl_token = req.headers["sl-token"];
        if (sl_token) {
            res.locals.isAdmin = await authorize_admin(sl_token);
            if (res.locals.isAdmin)
                console.log("Admin autorizado");
            else
                console.log("Admin no autorizado");
            next();
        }
    } catch {
        res.status(401).json({
            error: new Error('Si estas viendo esto, Houston, tenemos un problema! (auth_admin)')
        });
    }
};