Manual de Usuario App Server
75.52 - Taller de programación II
1° Cuatrimestre 2020


Grupo 5:

Ayudante: Agustin Rojas

Integrantes:
Beroch Santiago - 101135 
Frutos Ramos Constanza Micaela -  96728
Iribarren Alvaro Patricio - 101049
Nitz Ignacio - 100710


Fecha de entrega: 30/07/2020


Introducción
Este documento explica cómo instalar, configurar y ejecutar el servidor App Server de Chotuve. Este es un servidor backend que brinda con los datos tanto a la aplicación como al Web Admin. Se le pueden solicitar datos de usuarios, videos, comentarios, reacciones entre los más importantes
Expone una API REST para realizar estas acciones.
El stack de tecnologías usado en este servidor incluye:
Node JS como lenguaje de programación.
Express como servidor web.
PostgreSQL como base de datos SQL para persistencia.
Joi para la validación de los datos.
Axios para generar las requests HTTP.
Mocha y Chai para los tests.
Nools como motor de reglas.

Se hace uso de Gitlab para versionar el desarrollo del servidor.
Instalación de dependencias y ejecución
Para ambas tareas se utiliza el gestor de paquetes “npm”. No hace falta instalarlo ya que viene incluido con la misma descarga de Node JS. Para descargar node se puede ingresar al sitio oficial (https://nodejs.org/en/download/). En el conjunto de archivos se incluye uno llamado “package.json” donde se muestran todas las librerías requeridas para el correcto funcionamiento del servidor. Se pueden descargar todas con “npm install” (puede que se tenga que incluir sudo para proporcionar los correctos permisos).
Ejecución
La ejecución misma también se encuentra incluida en el archivo “package.json”. Esta se encuentra en forma de script bajo el nombre “start”. Lo que podemos hacer con esto es ingresar en la consola: 
npm start
Y ya tendremos el servidor corriendo localmente.
Tests
De la misma forma que se ejecutó el servidor localmente, se podrían querer probar las pruebas junto con una conexión a una base de datos local para poder ver los errores que se cometen en el desarrollo. Para esto mismo existe un script llamado “tests”. Como no es llamado “start” se debe anteponer la palabra “run” para asegurar el funcionamiento.
npm run tests
Y automáticamente se comenzarán a correr todas las pruebas de forma local. Podríamos querer averiguar el coverage de dichas pruebas, para esto existe el script “coverage”.
npm run coverage
Y obtendré, de mano de la librería “nyc” un informe del coverage separado por archivo.
Coverage
Se puede obtener de una forma más detallada utilizando:
npm run coverage:report


Autorización
Para solicitar los recursos del servidor no será posible la utilización de aplicaciones como Postman, al menos no de forma directa, ya que se requieren ciertos headers para que la requests sea aceptada. Algunos de estos son:
Token del servidor: De no tenerlo, será imposible la comunicación con el servidor mismo.
Tokens de usuario:
Short-Lived Token: Token de vida corta que tiene cada usuario. De vencerse debe renovarse a través del “Refresh token”.
Refresh Token: Token fijo, se envía para obtener un “Short-Lived Token” actualizado.

