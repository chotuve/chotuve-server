async function removeDisabledElements(elements) {
    return elements.filter(actualElement => actualElement.enabled);
}

module.exports = removeDisabledElements;