const express = require("express")
const morgan = require("morgan")
const usersRoute = require("./routes/users")
const friendsRoute = require("./routes/friends")
const videosRoute = require("./routes/videos")
const requestsRoute = require("./routes/requests")
const commentsRoute = require("./routes/comments")
const reactionsRoute = require("./routes/reactions")
const messagesRoute = require("./routes/messages")
const pingRoute = require("./routes/ping")
const loginRoute = require("./routes/login")
const statusRoute = require("./routes/status")
const tokenRoute = require("./routes/tokens")
const unknownRoute = require("./routes/unknown")
const cors = require('cors')
const madge = require('madge');
const port = process.env.PORT || 5000
const app = express();

//  SWAGGER
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: "Chotuve App Server",
            description: "API Info",
            contact: {
                name: "Iribarren Álvaro",
            },
            servers: ["http://localhost:5000"]
        }
    },
    apis: ['Middleware/*.js', 'routes/*.js']
}
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// MADGE: Chequeo de referencias circulares.
madge('./index.js').then((res) => {
    console.log(res.circular());
});

const bodyParser = require("body-parser")

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Seteando el servidor a escuchar en el puerto
app.listen(port, ()=>{
    console.log("Servidor escuchando en el puerto:", port);
});
app.set("views", __dirname + "/views");

//Agregado de cada request ejecutada a la tabla.
const RequestTableManager = require("./Managers/RequestTableManager")
app.use(morgan(function (tokens, req, res) {
        const str =  [
            tokens.method(req, res),
            tokens.url(req, res), 'with status',
            tokens.status(req, res), 'with response length',
            tokens.res(req, res, 'content-length'), '-',
            tokens['response-time'](req, res), 'ms'
        ].join(' ');
        RequestTableManager.insertRequest(req,res,tokens).then();
        return str;
    })
);

// ROUTES
app.use("/users", usersRoute);
app.use("/friends", friendsRoute);
app.use("/videos", videosRoute);
app.use("/requests", requestsRoute);
app.use("/comments", commentsRoute);
app.use("/reactions", reactionsRoute);
app.use("/messages", messagesRoute);
app.use("/ping", pingRoute);
app.use("/login", loginRoute);
app.use("/status", statusRoute);
app.use("/tokens", tokenRoute);
app.use("*", unknownRoute);


