const express = require('express')
const router = express.Router();
const CommentManager = require("../Managers/CommentsManager")
const VideoManager = require("../Managers/Videos/VideosManager")
const auth = require("../Middleware/auth")

async function validateVideoInfo(video_id) {
    return await VideoManager.getVideoById(video_id);
}

/**
 * @swagger
 * definitions:
 *  Comment:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id del comentario
 *              author_id:
 *                  type: integer
 *                  description: Id del usuario que realiza el comentario
 *              comment:
 *                  type: string
 *                  description: Comentario en sí
 * 
 */

 /**
 * @swagger
 * definitions:
 *  CommentWithName:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id del comentario
 *              author_id:
 *                  type: integer
 *                  description: Id del usuario que realiza el comentario
 *              author_name:
 *                  type: string
 *                  description: Nombre del usuario obtenido de otra tabla.
 *              comment:
 *                  type: string
 *                  description: Comentario en sí
 * 
 */

/**
 * @swagger
 * /comments:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - comments
 *      summary: Get all comments from all users
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/CommentWithName'
 */

router.get("/", auth, async (req, res) => {
    const comments = await CommentManager.getAllComments();
    res.send(comments);
})

/**
 * @swagger
 * /comments/{commentId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - comments
 *      summary: Get comment by id
 *      parameters:
 *      -   name: commentId
 *          description: Id del comentario a obtener.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns comment
 *              schema:
 *                  $ref: '#/definitions/CommentWithName'
 */

router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const relation = await CommentManager.getCommentByItsId(id);
    res.send(relation);
})

/**
 * @swagger
 * /comments:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - comments
 *      summary: Post a new comment
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: comment's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Comment'            
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Comment'
 */
router.post("/", auth, async (req, res) => {
    const error = await CommentManager.validateInput(req.body).error;
    if (!error) {
        const video_id = req.body.video_id;
        const rightVideoInfo = await validateVideoInfo(video_id);
        if (rightVideoInfo){
            const author_id = req.body.author_id;
            const comment = req.body.comment;
            const data = {author_id, video_id, comment};
            await CommentManager.postComment(data, res);
        }
    } else {
        res.status(400).send(error.details[0].message);
    }
})

module.exports = router;
