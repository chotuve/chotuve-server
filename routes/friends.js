const express = require('express')
const router = express.Router();
const FriendsManager = require("../Managers/FriendsManager")
const auth = require("../Middleware/auth")


/**
 * @swagger
 * definitions:
 *  Relation:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id de la amistad
 *              id1:
 *                  type: integer
 *                  description: Id del primer usuario de la amistad
 *              id2:
 *                  type: integer
 *                  description: Id del segundo comentario de la amistad
 * 
 */

 /**
 * @swagger
 * /friends:
 *  get:
  *      security:
  *          - Bearer:
  *              - user
 *      tags:
 *      - friends
 *      description: Get all relations
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Relation'
 */

router.get("/", auth, async (req, res) => {
    const relations = await FriendsManager.getRelations();
    res.send(relations);
})

/**
 * @swagger
 * /friends/{relationId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - friends
 *      summary: Get relation by id
 *      parameters:
 *      -   name: commentId
 *          description: Id de la relación a obtener.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns Value
 *              schema:
 *                  $ref: '#/definitions/Relation'
 *          '404':
 *              description: Value not found
 * 
 */
router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const relation = await FriendsManager.getRelationById(id);
    res.send(relation);
})

/**
 * @swagger
 * /friends:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - friends
 *      summary: Post a new relation
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: object's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Relation'            
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Relation'
 */
 
router.post("/", auth, async (req, res) => {
    const error = await FriendsManager.validateInput(req.body).error;
    if(!error)
        await FriendsManager.postRelation(req.body, res);
})


module.exports = router;
