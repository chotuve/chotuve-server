const express = require('express');
const router = express.Router();
const LoginManager = require("../Managers/LoginManager")
const UserManager = require("../Managers/Users/UsersManager")

const server_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTQ4NTM0MzIsIm5iZiI6MTU5NDg1MzQz" +
    "MiwianRpIjoiZWMwYWIxZWMtNmIxNS00NGNjLWE0ZTItNWI4ZWE1OTZjOTZkIiwiaWRlbnRpdHkiOiJodHRwczovL2Nob" +
    "3R1dmUtYXBwbGljYXRpb24tc2VydmVyLmhlcm9rdWFwcC5jb20vIiwidHlwZSI6InJlZnJl" +
    "c2giLCJ1c2VyX2NsYWltcyI6eyJzZXJ2ZXIiOnRydWV9fQ.klLra18fVSmrwKhENMGiLskZ5z2a8pRLEymBDZmVwnw";


/**
 * @swagger
 * definitions:
 *  LoginResponse:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: User's id 
 *              sl-token:
 *                  type: string
 *                  description: Token de vida corta del usuario que utilizará para autenticarse.
 *              refresh-token:
 *                  type: string
 *                  description: Token para poder obtener un nuevo token de vida corta.
 * 
 */    

 /**
 * @swagger
 * /login:
 *  post:
 *      tags:
 *      - login
 *      summary: Log in for user.
 *      description: Checks if the users exists, if he doesn't exist, he is created, if he does then the firebase_token is sent to the auth server to get the tokens.
 *      parameters:
 *      -   name: email
 *          type: string
 *          description: user's email to check his existance.
 *      -   name: firebase_token
 *          type: string
 *          description: user's token given by firebase.           
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#definitions/LoginResponse'
 *          '401':   
 *              description: Unauthorized, Invalid login.
 */

router.post("/", async (req, res) => {
    try {
        const email = req.body.email;
        const user = await UserManager.getUserByEmail(email);
        if (!user) {
            //No se encontro el user, creo uno nuevo desde login.
            await UserManager.postUser(req.body, res);
        } else {
            const actualUser = await UserManager.getUserById(user.id);
            if (actualUser.enabled) {
                const firebase_token = req.body.firebase_token;
                const headers = {
                    "App-Server-Api-Key": server_token,
                    "Firebase-Token": firebase_token
                }
                const tokens_and_id = await LoginManager.getTokensFromCreatedUser(headers);
                if (tokens_and_id){
                    const id = user.id;
                    await UserManager.updateLastLogin(id);          //actualiza la nueva fecha de login
                    tokens_and_id.id = id;
                    res.send(tokens_and_id);
                } else {
                    res.status(401).send("Fallo en el login, no se recibio nada del auth server");
                }
            } else {
                res.status(403).send("Usuario deshabilitado");
            }
        }
    } catch {
        res.status(401).send("Invalid request from created user");
    }
})

module.exports = router;
