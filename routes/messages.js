const express = require('express')
const router = express.Router();
const MessageManager = require("../Managers/MessagesManager")
const auth = require("../Middleware/auth")

/**
 * @swagger
 * definitions:
 *  Message:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Message's id.
 *              sender_id:
 *                  type: integer
 *                  description: User's id that sent the message.
 *              receiver_id:
 *                  type: integer
 *                  description: User's id who must receive the message.
 *              message:
 *                  type: string
 *                  description: Text to be sent.
 *              time:
 *                  type: string
 *                  format: date-time
 *                  description: Date and hour that the message was sent.
 *                  default: now().
 *
 */


/**
 * @swagger
 * /messages:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - messages
 *      summary: Get all messages from all users.
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Message'
 */
router.get("/", auth, async (req, res) =>{
    try {
        const users = await MessageManager.getAllMessages();
        console.log(users);
        res.send(users);
    } catch (err) {
        console.error(err);
        res.send("Error: " + err);
    }
})

/**
 * @swagger
 * /messages:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - messages
 *      summary: Post a new message
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: Message's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Message'
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Message'
 */
router.post("/", auth, async (req, res) => {
    try{
        await MessageManager.postMessage(req.body, res);
    } catch (err) {
        console.error(err);
        res.send("ERROR:" + err);
    }
})

module.exports = router;