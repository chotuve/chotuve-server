const express = require('express');
const router = express.Router();

/**
 * @swagger
 * /ping:
 *  get:
 *      tags:
 *      - ping
 *      summary: used to know whether the server is running or not.
 *      responses:
 *          '200':
 *              description: Successful response
 */
router.get("/", async (req,res) => {
    res.status(200).send({msg: "WORKING"});
})

module.exports = router;