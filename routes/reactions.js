const express = require('express')
const router = express.Router();
const ReactionManager = require("../Managers/Reactions/ReactionsManager")
const postReaction = require("../Managers/Reactions/ReactionsIndex")
const VideoManager = require("../Managers/Videos/VideosManager")
const auth = require("../Middleware/auth")

async function validateVideosExistance(video_id){
    return await VideoManager.getVideoById(video_id);
}

/**
 * @swagger
 * definitions:
 *  Reaction:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id de la reacción.
 *              author_id:
 *                  type: integer
 *                  description: Id del usuario que realiza el reacción.
 *              positive_reaction:
 *                  type: boolean
 *                  description: Valor booleano de la reacción, si verdadero, opinión positiva.
 * 
 */

  /**
 * @swagger
 * definitions:
 *  ReactionWithName:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id de la reacción
 *              author_id:
 *                  type: integer
 *                  description: Id del usuario que realiza la reacción
 *              author_name:
 *                  type: string
 *                  description: Nombre del usuario obtenido de otra tabla.
 *              positive_reaction:
 *                  type: string
 *                  description: Valor booleano de la reacción, si verdadero, opinión positiva.
 * 
 */

 /**
 * @swagger
 * /reactions:
 *  get:
  *      security:
  *          - Bearer:
  *              - user
 *      tags:
 *      - reactions
 *      summary: Get all reactions from all users
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  description: Many reactions, all with the author_name included
 *                  items:
 *                      $ref: '#/definitions/ReactionWithName'
 */
router.get("/", auth, async (req, res) => {
    const relations = await ReactionManager.getAllReactions();
    res.send(relations);
})


/**
 * @swagger
 * /reactions/{reactionId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - reactions
 *      summary: Get reaction by id
 *      parameters:
 *      -   name: reactionId
 *          description: Id de la reacción a obtener.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns a reaction
 *              schema:
 *                  $ref: '#/definitions/ReactionWithName'
 */
router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const relation = await ReactionManager.getReactionById(id);
    res.send(relation);
})

/**
 * @swagger
 * /reactions:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - reactions
 *      summary: Post a new reaction
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: Reactions's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Reaction'            
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Reaction'
 */
router.post("/", auth, async (req, res) => {
    await ReactionManager.validateInput(req.body);
    const videoExists = validateVideosExistance(parseInt(req.body.video_id));
    if (videoExists)
        await postReaction(req.body, res);
})

module.exports = router;
