const express = require('express')
const router = express.Router();
const RequestManager = require("../Managers/FriendRequestManager")
const auth = require("../Middleware/auth")
const Joi = require("joi")

/**
 * @swagger
 * definitions:
 *  Request:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Id de la solicitud.
 *              sender_id:
 *                  type: integer
 *                  description: Id del usuario que envía la solicitud de amistad.
 *              receiver_id:
 *                  type: integer
 *                  description: Id del usuario que recibe la solicitud de amistad.
 * 
 */

 /**
 * @swagger
 * /requests:
 *  get:
  *      security:
  *          - Bearer:
  *              - user
 *      tags:
 *      - requests
 *      summary: Get all requests from all users
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Request'
 */
router.get("/", auth, async (req, res) => {
    const requests = await RequestManager.getRequests();
    res.send(requests);
})

/**
 * @swagger
 * /requests/{requestId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - requests
 *      summary: Get request by id
 *      parameters:
 *      -   name: requestId
 *          description: Id de la request a obtener.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns request.
 *              schema:
 *                  $ref: '#/definitions/Request'
 *          '404':
 *              description: Request not found
 */
router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const relation = await RequestManager.getRequestById(id);
    res.send(relation);
})

 router.get("/:senderid/:receiverid", auth, async (req, res) => {
    console.log("You asked for a certain request between users")
    const request = await RequestManager.getRequestByUsersIds(req.params.senderid, req.params.receiverid);
    res.send(request);
    console.log(request);
})

async function validateInput(body){
    const schema = {
        sender_id: Joi.number().positive().required(),
        receiver_id: Joi.number().positive().required()
    }

    return Joi.validate(body, schema);
}

/**
 * @swagger
 * /requests:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *              - user
 *      tags:
 *      - requests
 *      summary: Post a new request
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: requests's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Request'            
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Request'
 */

router.post("/", auth, async (req, res) => {
    const error = await validateInput(req.body).error;
    if (!error){
        await RequestManager.postRequest(req.body, res);
    }
})

//refactor
router.delete("/:senderid/:receiverid", auth, async (req,res) => {
    const sender_id = parseInt(req.params.id1);
    const receiver_id = parseInt(req.params.id2);

    await RequestManager.deleteRequestFromSenderToReceiver(sender_id, receiver_id);
    res.send("Relation deleted");
})

module.exports = router;