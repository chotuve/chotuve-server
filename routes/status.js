const express = require('express');
const router = express.Router();
const UserManager = require("../Managers/Users/UsersManager")
const VideoManager = require("../Managers/Videos/VideosManager")
const SearchManager = require("../Managers/SearchManager")
const RequestTableManager = require("../Managers/RequestTableManager")
const auth = require('../Middleware/auth');
const auth_admin = require('../Middleware/auth_admin');

/**
 * @swagger
 * definitions:
 *  HTTP_Request:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: id from the search.
 *              method:
 *                  type: string
 *                  description: request's type.
 *              url:
 *                  type: string
 *                  description: amount of times the word's been searched.
 *              status:
 *                  type: integer
 *                  description: returned status from app server.
 *              res_length:
 *                  type: integer
 *                  description: response's length.
 *              res_time:
 *                  type: integer
 *                  description: amount of milliseconds the server took to answer the request.
 *              upload_date:
 *                  type: string
 *                  description: date the request was made.
 *
 */

/**
 * @swagger
 * definitions:
 *  Search:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: id from the search.
 *              word:
 *                  type: string
 *                  description: the word that's been searched.
 *              amount:
 *                  type: integer
 *                  description: amount of times the word's been searched.
 *
 */

/**
 * @swagger
 * definitions:
 *  Report:
 *      type: object
 *      properties:
 *              amountOfUsers:
 *                  type: integer
 *                  description: total amount of users including inactive and disabled users.
 *              amountOfVideos:
 *                  type: integer
 *                  description: total amount of videos including the disabled ones.
 *              searches:
 *                  type: array
 *                  description: Array of all searches.
 *                  items:
 *                      $ref: '#definitions/Search'
 *              activeUsers:
 *                  type: array
 *                  description: array with all active users. An active user is someone who was logged into the app in the last 3 days.
 *                  items:
 *                      $ref: '#definitions/User'
 *              totalViews:
 *                  type: integer
 *                  description: sum of views from ALL videos, including the disabled ones.
 *              requests:
 *                  type: array
 *                  description: all requests ever made to the app server.
 *                  items:
 *                      $ref: '#definitions/HTTP_Request'
 *
 */


/**
 * @swagger
 * /status:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - status
 *      summary: Get a full status report
 *      responses:
 *          '200':
 *              description: Returns full report.
 *              schema:
 *                  $ref: '#/definitions/Report'
 */
router.get("/", auth, auth_admin, async (req, res) => {
    try {
        const amountOfUsers = await UserManager.getAmountOfUsers();
        const amountOfVideos = await VideoManager.getAmountOfVideos();
        const searches  = await SearchManager.getSearches();
        const active_users = await UserManager.getActiveUsers();
        const totalViews = await VideoManager.getTotalViews();
        const requests = await RequestTableManager.getRequests();
        const data = {amountOfUsers, amountOfVideos, searches, active_users, totalViews, requests};
        res.send(data);
    } catch {
        res.status(401).send("Invalid request from created user, probably no fb token");
    }
})

module.exports = router;