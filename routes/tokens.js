const express = require('express')
const router = express.Router();
const TokenManager = require("../Managers/TokensManager")
const auth = require('../Middleware/auth')

router.get("/", auth, async (req, res) => {
    const relations = await TokenManager.getAllTokens();
    res.send(relations);
})

router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const token = await TokenManager.getTokenById(id);
    res.send(token);
})

router.post("/", auth, async (req, res) => {
    await TokenManager.validateInput(req.body);
    await TokenManager.postToken(req.body, res);
})

module.exports = router;