const express = require('express')
const router = express.Router();
const UserManager = require("../Managers/Users/UsersManager")
const FriendManager = require("../Managers/FriendsManager")
const VideosManager = require("../Managers/Videos/VideosManager")
const TokenManager = require("../Managers/TokensManager")
const FriendRequestManager = require("../Managers/FriendRequestManager")
const MessageManager = require("../Managers/MessagesManager")
const CommentManager = require("../Managers/CommentsManager")
const ReactionManager = require("../Managers/Reactions/ReactionsManager")
const auth = require("../Middleware/auth")
const auth_admin = require("../Middleware/auth_admin")

/**
 * @swagger
 * definitions:
 *  UserWithoutInfo:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: user's Id
 *              img_id:
 *                  type: integer
 *                  description: img id from media server.
 *              last_login:
 *                  type: string
 *                  format: date-time
 *                  description: last login from user.
 *                  default: now
 *              enabled:
 *                  type: boolean
 *                  default: true
 *                  description: Wether the video is available or has been disabled by the admin.
 *
 */

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: user's Id
 *              img_id:
 *                  type: integer
 *                  description: img id from media server.
 *              last_login:
 *                  type: string
 *                  format: date-time
 *                  description: last login from user.
 *                  default: now
 *              enabled:
 *                  type: boolean
 *                  default: true
 *                  description: Wether the video is available or has been disabled by the admin.
 *              name:
 *                  type: string
 *                  description: user's name.
 *              email:
 *                  type: string
 *                  format: email
 *                  description: user's email
 *              phone:
 *                  type: string
 *                  description: user's phone number.
 *              sign_in_method:
 *                  type: string
 *                  description: method used by the user to log into the app. (google, facebook, etc).
 *              img_url:
 *                  type: string
 *                  description: user profile picture's url.
 *              img_uuid:
 *                  type: string
 *                  description: profile picture's uuid.
 *
 */

/**
 * @swagger
 * /users:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Get all users with added information from auth and media servers.
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/User'
 */
router.get("/", auth, auth_admin, async (req, res) =>{
    try {
        const isAdmin = res.locals.isAdmin;
        if (isAdmin) {
            const allUsers = await UserManager.getUsers();
            res.send(allUsers);
        } else {
            const allUsers = await UserManager.getEnabledUsers();
            res.send(allUsers);
        }
    } catch (err) {
        console.error(err);
        res.send("Error: " + err);
    }
})


/**
 * @swagger
 * /users/{userId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: get a user with added info by id.
 *      parameters:
 *      -   name: userId
 *          description: user's id.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns user with info
 *              schema:
 *                  $ref: '#/definitions/User'
 *          '404':
 *              description: user not found
 */
router.get("/:id", auth, async (req, res) =>{
    const id = parseInt(req.params.id);
    const user = await UserManager.getUserById(id);
    if (user)
        res.send(user);
    else
        res.status(404).send("User not found");
})

/**
 * @swagger
 * /users/{userId}/friends:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Get all friends from a given user id.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user's id.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Relation'
 *          '404':
 *              description: User id not found
 */
router.get("/:id/friends", auth, async (req, res) => {
    const userId = parseInt(req.params.id);
    const userExists = await UserManager.getUserById(userId);
    if (userExists){
        const friends = await FriendManager.getAllFriendsFromUser(userId);
        res.send(friends);
    } else {
        res.status(404).send("User with id: " + userId + " not found.");
    }
})

/**
 * @swagger
 * /users/{userId}/videos:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Get all videos from "userId" with their urls from media server.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user who's videos are about to be shown.
 *      -   name: requesterId
 *          in: header
 *          description: user who requests videos. If this id is equal to "userId" then private videos are also sent.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Video'
 *          '404':
 *              description: userId not found.
 */
router.get("/:id/videos", auth, auth_admin, async (req, res) => {
    const requester_id = parseInt(req.header("requester_id"));
    const userId = parseInt(req.params.id);
    const userExists = await UserManager.getUserById(userId);
    const isAdmin = res.locals.isAdmin;
    if (userExists){
        const showPrivateVideos = (isAdmin || requester_id === userId);
        const videos = await VideosManager.getAllVideosFromUser(userId, showPrivateVideos);
        res.send(videos)
    } else {
        res.status(404).send("User with id: " + userId + " not found.");
    }
})

//pre: user exists.
//post: sends token from user.
router.get("/:id/token", auth, async (req, res) => {
    const userId = parseInt(req.params.id);
    const userExists = await UserManager.getUserById(userId);
    if (userExists){
        const token = await TokenManager.getTokenByUserId(userId);
        res.send(token)
    } else {
        res.status(404).send("User with id: " + userId + " not found.");
    }
})

/**
 * @swagger
 * /users/{userId1}/messages/{userId2}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Get all messages sent by "userId1" to "userId2".
 *      parameters:
 *      -   name: userId1
 *          in: path
 *          description: user who's videos are about to be shown.
 *          required: true
 *      -   name: userId2
 *          in: path
 *          description: user who's videos are about to be shown.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Message'
 *          '404':
 *              description: at least one of the users was not found.
 */
router.get("/:id1/messages/:id2", auth, async (req,res) => {
    const id1 = parseInt(req.params.id1);
    const id2 = parseInt(req.params.id2);

    const user1Exists = await UserManager.getUserById(id1);
    const user2Exists = await UserManager.getUserById(id2);
    if (user1Exists && user2Exists){
        const messages1 = await MessageManager.getAllMessagesSentById1ToId2(id1, id2);
        const messages2 = await MessageManager.getAllMessagesSentById1ToId2(id2, id1);
        const messages = messages1.concat(messages2);
        res.send(messages);
    } else {
        res.status(404).send("User with id: " + id1 + " or " + id2 + " not found.");
    }
})

/**
 * @swagger
 * /users/{receiverId}/requests:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Get all friend requests received by user "receiverId".
 *      parameters:
 *      -   name: receiverId
 *          in: path
 *          description: user who's received requests are about to be returned.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      properties:
 *                          sender_id:
 *                              type: string
 *                              description: user id who's sent a friend request to {receiverId}.
 *                          sender_name:
 *                              type: string
 *                              description: sender's name.
 *          '404':
 *              description: at least one of the users was not found.
 */
router.get("/:receiver_id/requests", auth, async (req,res) => {
    const receiver_id = parseInt(req.params.receiver_id);
    const requests = await FriendRequestManager.getAllRequestsReceivedByUserId(receiver_id);
    const listToReturn = [];
    for (let request of requests){
        const user = await UserManager.getUserById(request.sender_id);
        const data = {
            "sender_id": request.sender_id,
            "sender_name": user.name
        }
        listToReturn.push(data);
    }
    res.send(listToReturn);
});

/**
 * @swagger
 * /users:
 *  post:
 *      tags:
 *      - users
 *      summary: Post a new user
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: user's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/User'
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Comment'
 */
router.post('/', async (req, res) => {
    const error = await UserManager.validateUser(req.body).error;
    if (!error) {
        await UserManager.postUser(req.body, res);
    } else {
        res.status(400).send(error.details[0].message);
    }
})

/**
 * @swagger
 * /users/{receiverId}/friends:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Post a new friendship between the users. There must be an existing friend request from one of them to the other.
 *      parameters:
 *      -   name: sender_id
 *          in: body
 *          description: user's id who's sent the friend request.
 *          required : true
 *      -   name: receiver_id
 *          in: path
 *          description: user's id who's received the friend request
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Comment'
 *          '400':
 *              description: Invalid data.
 *          '404':
 *              description: at least one of the users was not found.
 */
router.post('/:receiver_id/friends', auth, async (req, res) => {
    const data = {
        id1: parseInt(req.params.receiver_id),  //id1: received/accepted request
        id2: parseInt(req.body.sender_id)       //id2: sent request
    }
    const error = await FriendManager.validateInput(data).error;
    if (!error){
        await FriendManager.postRelation(data, res);
    } else {
        res.status(400).send("Datos invalidos o inexistentes");
    }
})

/**
 * @swagger
 * /users/{receiverId}/requests:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: Post a new friend request from sender id to receiver_id between the users.
 *      parameters:
 *      -   name: sender_id
 *          in: body
 *          description: user's id who's sent the friend request.
 *          required : true
 *      -   name: receiver_id
 *          in: path
 *          description: user's id who's received the friend request
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '404':
 *              description: at least one of the users was not found.
 */
router.post("/:receiver_id/requests", auth, async (req, res)=> {
    const data = {
        sender_id: parseInt(req.body.sender_id),
        receiver_id: parseInt(req.params.receiver_id)
    }
    await FriendRequestManager.postRequest(data, res);
});

/**
 * @swagger
 * /users/{userId}/image:
 *  put:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - users
 *      summary: modifies {userId} profile picture.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user's Id.
 *          required: true
 *      -   name: requester_id
 *          in: header
 *          type: integer
 *          description: user who wants to modify the profile picture.
 *      responses:
 *          '200':
 *              description: Successful operation.
 *              schema:
 *                  type: object
 *                  properties:
 *                      id:
 *                          type: integer
 *                      img_url:
 *                          type: string
 *                          format: URL
 *                          description: new img url.
 *                      img_uuid:
 *                          type: string
 *                          format: URL
 *                          description: new img_uuid.
 *          '400':
 *              description: Unknown error.
 *          '401':
 *              description: User not allowed to modify this profile.
 *          '404':
 *              description: User not found.
 */
router.put('/:id/image', auth, auth_admin, async (req,res) => {
    const id = parseInt(req.params.id);
    const error = await UserManager.validateImageModification(req.body).error;
    const user = await UserManager.getUserById(id);
    const requester_id = parseInt(req.headers["requester-id"]);
    const isAdmin = res.locals.isAdmin;
    const hasPermissionToModify = (isAdmin || requester_id === user.id);

    if (!error && user && hasPermissionToModify){
        const img_id = user.img_id;
        const img_url = req.body.img_url;
        const img_uuid = req.body.img_uuid;
        const result = await UserManager.changeProfilePicture(id, img_id, img_url, img_uuid);
        if (result){
            res.status(200).send({id, img_url, img_uuid});
        } else {
            res.status(400).send("Something failed :)");
        }
    } else {
        if (!hasPermissionToModify){
            res.status(401).send("Flaco no podes modificar un perfil que no es tuyo");
        } else {
            res.status(404).send(error.details[0].message);
        }
    }
})

/**
 * @swagger
 * /users/{userId}/profile:
 *  put:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - users
 *      summary: modifies {userId} profile.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user's Id.
 *          required: true
 *      -   name: requester_id
 *          in: header
 *          type: integer
 *          description: user who wants to modify the profile picture.
 *      -   name: body
 *          in: body
 *          schema:
 *              type: object
 *              properties:
 *                  name:
 *                      type: string
 *                      description: new name for user.
 *                  email:
 *                      type: string
 *                      description: new email for user
 *                  phone:
 *                      type: string
 *                      description: new phone number for user.
 *      responses:
 *          '200':
 *              description: Successful operation.
 *          '400':
 *              description: Unknown error.
 *          '401':
 *              description: User not allowed to modify this profile.
 *          '404':
 *              description: User not found.
 */
router.put('/:id/profile', auth, auth_admin, async (req, res) => {
    const id = parseInt(req.params.id);
    const error = await UserManager.validateProfileModification(req.body).error;
    const userExists = await UserManager.doesUserExist(id);
    const requester_id = parseInt(req.headers["requester-id"]);
    const isAdmin = res.locals.isAdmin;
    const hasPermissionToModify = (isAdmin || requester_id === userExists.id);

    if(!error && userExists && hasPermissionToModify){
        const data = {
            display_name: req.body.name,
            email: req.body.email,
            phone_number: req.body.phone
        }
        const result = await UserManager.editUser(id, data);
        res.status(200).send(result);
    } else {
        res.status(400).send("Error en validacion: " + error.details[0].message);
    }
});


/**
 * @swagger
 * /users/{userId}/enabled:
 *  put:
 *      security:
 *          - Bearer:
 *              - admin
 *      tags:
 *      - users
 *      summary: if user is initially enabled, he is disabled. Otherwise, he is enabled.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user's Id.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful operation.
 *          '401':
 *              description: User not allowed to modify this value.
 *          '404':
 *              description: User not found.
 */
router.put("/:user_id/enabled", auth, auth_admin, async (req,res) => {
    if (res.locals.isAdmin) {
        const user_id = parseInt(req.params.user_id);
        const userExists = await UserManager.doesUserExist(user_id);
        const enabled = await UserManager.isUserEnabled(user_id);
        if (userExists && enabled) {
            await UserManager.disableUser(user_id);
            res.send("User disabled");
        } else if (userExists && !enabled){
            await UserManager.enableUser(user_id);
            res.send("User enabled");
        } else {
            res.status(404).send("User with id: " + user_id + " not found");
        }
    } else {
        res.status(401).send("Se necesitan permisos de admin para realizar esta acción");
    }
})

/**
 * @swagger
 * /users/{userId}:
 *  delete:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - users
 *      summary: deletes user and his reactions, comments, videos, and everything related to him.
 *      parameters:
 *      -   name: userId
 *          in: path
 *          description: user's Id.
 *          required: true
 *      -   name: requesterId0
 *          in: header
 *          type: integer
 *          description: it's used to know who tried to delete the user. A user is allowed to delete his own account but not others'.
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '401':
 *              description: User not allowed to delete this account.
 *          '404':
 *              description: Video not found
 */
router.delete('/:id', auth, auth_admin, async (req, res) => {
    const requester_id = parseInt(req.headers["requester-id"]);
    const id = parseInt(req.params.id);
    const userAllowed = requester_id === id;
    const isAdmin = res.locals.isAdmin;
    if (isAdmin || userAllowed) {
        await UserManager.deleteUserById(id);
        await VideosManager.deleteAllVideosFromUser(id);
        await CommentManager.deleteAllCommentsFromUsers(id);
        await ReactionManager.deleteAllReactionsFromUser(id);
        await MessageManager.deleteAllMessagesWithUserInvolved(id);
        await FriendManager.deleteAllRelationsFromUser(id);
        await FriendRequestManager.deleteAllRequestsWhereUserIsInvolved(id);
        res.status(200).send("User eliminated");
    } else {
        console.log("No permission to delete user");
        res.status(401).send("User doesnt have permission to delete");
    }
})


/**
 * @swagger
 * /users/{userId1}/friends/{userId2}:
 *  delete:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - users
 *      summary: deletes users' friendship.
 *      parameters:
 *      -   name: userId1
 *          in: path
 *          description: user's 1 Id.
 *          required: true
 *      -   name: userId2
 *          in: path
 *          type: integer
 *          description: user's 2 Id.
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '401':
 *              description: User not allowed to delete this account.
 *          '404':
 *              description: Video not found
 */
router.delete('/:id1/friends/:id2', auth, auth_admin, async (req,res) => {
    const id1 = parseInt(req.params.id1);
    const id2 = parseInt(req.params.id2);
    const requester_id = parseInt(req.headers["requester-id"])
    const isAdmin = res.locals.isAdmin;

    if (isAdmin || id1 === requester_id || id2 === requester_id) {
        const positiveResult = await FriendManager.deleteRelationBetweenUsers(id1, id2);
        if (positiveResult) {
            await MessageManager.deleteAllMessagesBetweenUsers(id1, id2);
            res.send("Amistad eliminada");
        } else {
            res.status(404).send("Users not found or they weren't friends");
        }
    } else {
        console.log("User not allowed to delete this friendship");
        res.status(401).send("User not allowed to delete this friendship");
    }
})

module.exports = router;
