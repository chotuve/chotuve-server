const express = require('express');
const router = express.Router();
const UserManager = require("../Managers/Users/UsersManager")
const VideosManager = require("../Managers/Videos/VideosManager")
const CommentManager = require("../Managers/CommentsManager")
const ReactionManager = require("../Managers/Reactions/ReactionsManager")
const postReaction = require("../Managers/Reactions/ReactionsIndex")
const auth = require("../Middleware/auth")
const auth_admin = require("../Middleware/auth_admin")
const removeDisabled = require("../Utils/removeDisabled")

const Joi = require("joi")

async function noSearchQuery(search){
    return search === undefined || search === null || search === "" || search === " ";
}

const sortArray = require("../Utils/sortByImportance");
const removeImportance = require("../Utils/removeImportance");

/**
 * @swagger
 * definitions:
 *  Video:
 *      type: object
 *      properties:
 *              id:
 *                  type: integer
 *                  description: Video's Id
 *              author_id:
 *                  type: integer
 *                  description: User's id from the one that uploaded the video. 
 *              author_name:
 *                  type: string
 *                  description: User's name searched by the author_id field. 
 *              description:
 *                  type: string
 *                  description: Video's extra information for the user to add.
 *              title:
 *                  type: string
 *                  description: Video's title.
 *              location:
 *                  type: string
 *                  description: place where the video was recorder.
 *              public_video:
 *                  type: boolean
 *                  description: true if the video is public to everyone.
 *              likes:
 *                  type: integer
 *                  description: amount of people who liked the video.
 *              dislikes:
 *                  type: integer
 *                  description: amount of people who disliked the video.
 *              views:
 *                  type: integer
 *                  description: amount of people who have visited the video.
 *              upload_date:
 *                  type: string
 *                  format: date-time
 *                  default: now
 *              enabled:
 *                  type: boolean
 *                  default: true
 *                  description: Wether the video is available or has been disabled by the admin.
 *              url:
 *                  type: string
 *                  format: URL
 *                  description: video's url
 *              video_size:
 *                  type: integer
 *                  description: video's size in MB.
 * 
 */  

  /**
 * @swagger
 * /videos:
 *  get:
   *      security:
   *          - Bearer:
   *              - user
 *      tags:
 *      - videos
 *      summary: Get all videos with their urls from media server
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/Video'
 */
router.get("/", auth, auth_admin, async(req, res) => {
    console.log("Desde videos: " + res.locals.sl_token);
    console.log("Valor de admin:" + res.locals.isAdmin);
    const admin = res.locals.isAdmin;
    if (admin){
        //Si es admin le mando todos los videos
        const allVideos = await VideosManager.getVideos();
        res.send(allVideos);
    } else {
        const requester_id = req.headers["requester-id"];       //Me fijo quien solicita los videos
        const requester_exists = await UserManager.doesUserExist(requester_id);
        if (requester_exists) {
            //Actualizo el último login del usuario. Lo hago aca porque esta es la pantalla principal
            await UserManager.updateLastLogin(requester_id);
            if (res.locals.sl_token) {
                const sl_token = res.locals.sl_token;
                res.header({"Sl-Token": sl_token});
            }
            let search = req.query.search_query;
            let videos = await VideosManager.getVideosWithImportance(requester_id);
            videos = await removeDisabled(videos);
            if (await noSearchQuery(search)) {
                await sortArray(videos);
                await removeImportance(videos);
                await
                res.send(videos);
            } else {
                //Adentro se agrega la busqueda a la lista
                const filtratedVideos = await VideosManager.getSearchRelatedVideos(videos, search);
                await sortArray(filtratedVideos);
                await removeImportance(filtratedVideos);
                res.send(filtratedVideos);
            }
        }
    }
})

router.get("/appServer", auth, async (req,res)=>{
    const videosInAppSv = await VideosManager.getVideosInAppServer();
    res.send(videosInAppSv);
})

/**
 * @swagger
 * /videos/{videoId}:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - videos
 *      summary: Get video by id
 *      parameters:
 *      -   name: videoId
 *          description: video's id in app and media server.
 *          required: true
 *      responses:
 *          '200':
 *              description: Returns video with all information.
 *              schema:
 *                  $ref: '#/definitions/Video'
 *          '404':
 *              description: Value not found
 * 
 */
router.get("/:id", auth, async (req, res) => {
    const id = parseInt(req.params.id);
    const enabled = await VideosManager.isVideoEnabled(id);
    if (enabled) {
        await VideosManager.addViewToVideo(id);
        const video = await VideosManager.getVideoById(id);
        res.send(video);
    } else {
        res.status(403).send("Forbidden, video enabled");
    }
})


/**
 * @swagger
 * /videos/{videoId}/comments:
 *  get:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - videos
 *      summary: Get all comments from the video related to "videoId".
 *      parameters:
 *      -   name: videoId
 *          description: video's id in app and media server.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  items:
 *                      $ref: '#/definitions/CommentWithName'
 */
router.get("/:video_id/comments", auth, async (req, res) => {
    const video_id = parseInt(req.params.video_id);
    const comments = await CommentManager.getAllCommentsFromVideo(video_id);
    res.send(comments);
})

 /**
 * @swagger
 * /videos/{videoId}/reactions:
 *  get:
  *      security:
  *          - Bearer:
  *              - user
 *      tags:
 *      - videos
 *      summary: Get all reactions from the video related to "videoId".
 *      parameters:
 *      -   name: videoId
 *          description: video's Id.
 *          required: true
 *      responses:
 *          '200':
 *              description: Successful response
 *              schema:
 *                  type: array
 *                  description: Many reactions, all with the author_name included
 *                  items:
 *                      $ref: '#/definitions/ReactionWithName'
 */
router.get("/:video_id/reactions", auth, async(req, res) => {
    const video_id = parseInt(req.params.video_id);
    const reactions = await ReactionManager.getAllReactionsFromVideo(video_id);
    res.send(reactions);
})

async function validateInputForPost(body){
    const schema = {
        author_id: Joi.number().positive().required(),
        title: Joi.string().required(),
        description: Joi.string(),
        location: Joi.string(),
        public_video: Joi.required(),
        url: Joi.string().required(),
        uuid: Joi.required(),
        video_size: Joi.required()
    }
    return Joi.validate(body, schema);
}

async function validateModifyVideo(body){
    const schema = {
        author_id: Joi.number().positive().required(),
        title: Joi.string().required(),
        description: Joi.string().required(),
        location: Joi.string().required(),
        public_video: Joi.required()
    }
    return Joi.validate(body, schema);
}


/**
 * @swagger
 * /videos:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - videos
 *      summary: Post a new video
 *      parameters:
 *      -   in: body
 *          name: body
 *          description: object's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Video'            
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Video'
 */

router.post("/", auth, async (req, res) => {
    const error = await validateInputForPost(req.body).error;
    if (!error){
        console.log(req.body);
        await VideosManager.postVideo(req.body, res);
    } else {
        res.status(400).send(error.details[0].message);
    }
})

async function validateVideoInfo(video_id){
    const video = await VideosManager.doesVideoExist(video_id);
    return video.length > 0;
}



/**
 * @swagger
 * /videos/{videoId}/reactions:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - videos
 *      summary: Post a new reaction to a video
 *      parameters:
 *      -   name: videoId
 *          description: video's Id.
 *          required: true
 *      -   in: body
 *          name: body
 *          description: Reactions's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Reaction'
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Reaction'
 */
router.post("/:video_id/reactions", auth, async (req, res) => {
    const error = await ReactionManager.validateInput(req.body).error;
    if (!error) {
        const author_id = req.body.author_id;
        const video_id = parseInt(req.params.video_id);
        const positive_reaction = req.body.positive_reaction;
        const rightVideoInfo = await validateVideoInfo(video_id);
        const userExists = await UserManager.doesUserExist(author_id);
        if (userExists && rightVideoInfo) {
            const data = {author_id, video_id, positive_reaction};
            await postReaction(data, res);
        } else {
            res.status(400).send("Video or user doesn't exist");
        }
    } else {
        res.status(400).send(error.details[0].message);
    }
})

/**
 * @swagger
 * /videos/{videoId}/comments:
 *  post:
 *      security:
 *          - Bearer:
 *              - user
 *      tags:
 *      - videos
 *      summary: Post a new comment to a video
 *      parameters:
 *      -   name: videoId
 *          description: video's Id.
 *          required: true
 *      -   in: body
 *          name: body
 *          description: comment's atributes.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Comment'
 *      responses:
 *          '200':
 *              description: Successful operation
 *              schema:
 *                  $ref: '#/definitions/Comment'
 */
router.post("/:video_id/comments", auth, async (req, res) => {
    const error = await CommentManager.validateInput(req.body).error;
    if (!error) {
        const author_id = req.body.author_id;
        const video_id = parseInt(req.params.video_id);
        const comment = req.body.comment;
        const data = {author_id, video_id, comment};
        await CommentManager.postComment(data, res);
    } else {
        res.status(400).send(error.details[0].message);
    }
})



/**
 * @swagger
 * /videos/{videoId}:
 *  put:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - videos
 *      summary: Modifies video's atributes.
 *      description: Modifies existing video by its Id. If a video object has N atributes, this put method accepts from 1 to N atributes in the body for modifying. So in one PUT request, the whole video could be modified and in another just one atribute.
 *      parameters:
 *      -   name: videoId
 *          description: video's Id.
 *          required: true
 *      -   in: body
 *          name: body
 *          description: new video's atributes. Could be anyone except the video's id and it's information in the media server.
 *          required : true
 *          schema:
 *              $ref: '#definitions/Video'
 *      responses:
 *          '200':
 *              description: Successful operation
 */
router.put("/:video_id", auth, auth_admin, async (req,res) => {
    const error = await validateModifyVideo(req.body).error;
    if (!error) {
        const video_id = parseInt(req.params.video_id);
        const videoToModify = await VideosManager.getVideoWithNoUrlById(video_id);
        const modifiedVideo = await req.body;
        const requester_id = parseInt(req.headers["requester-id"]);
        const isAllowedToModify = (requester_id === videoToModify.author_id);
        const isAdmin = res.locals.isAdmin;

        //Si el video existe y si tiene permisos, ya sea por ser el propio usuario o por ser admin.
        if (videoToModify && (isAdmin || isAllowedToModify)) {
            for (let key in modifiedVideo) {
                if (modifiedVideo.hasOwnProperty(key)) {
                    if (modifiedVideo[key] !== videoToModify[key]) {
                        const newValue = modifiedVideo[key];
                        await VideosManager.modifiyVideo(video_id, key, newValue);
                    }
                }
            }
        }
        const video = await VideosManager.getVideoById(video_id);
        res.send(video);
    }
})

/**
 * @swagger
 * /videos/{videoId}/enabled:
 *  put:
 *      security:
 *          - Bearer:
 *              - admin
 *      tags:
 *      - videos
 *      summary: enables or disables video depending on actual state.
 *      parameters:
 *      -   name: videoId
 *          description: video's Id.
 *          required: true
 *          schema:
 *              $ref: '#definitions/Video'
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '404':
 *              description: Video not found
 */
router.put("/:video_id/enabled", auth, auth_admin, async (req,res) => {
    if (res.locals.isAdmin) {
        const video_id = parseInt(req.params.video_id);
        const enabled = await VideosManager.isVideoEnabled(video_id);
        if (enabled) {
            await VideosManager.disableVideo(video_id);
            res.send("Video disabled");
        } else {
            await VideosManager.enableVideo(video_id);
            res.send("Video enabled");
        }
    }
})

/**
 * @swagger
 * /videos/{videoId}:
 *  delete:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - videos
 *      summary: deletes video, it's reactions and comments.
 *      parameters:
 *      -   name: videoId
 *          in: path
 *          description: video's Id.
 *          required: true
 *      -   name: requesterId
 *          in: header
 *          type: integer
 *          description: it's used to know who tried to delete the video. A user is allowed to delete his videos but not videos from other users.
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '401':
 *              description: User not allowed to delete this video.
 *          '404':
 *              description: Video not found
 */
router.delete("/:video_id", auth, auth_admin, async (req,res) => {
    const requester_id = parseInt(req.headers["requester-id"]);
    const video_id = parseInt(req.params.video_id);
    const videoExists = await VideosManager.getVideoById(video_id);
    const author_id = videoExists.author_id;
    const userAllowedToDelete = requester_id === author_id;
    const isAdmin = res.locals.isAdmin;
    if (videoExists && (isAdmin || userAllowedToDelete)){
        const result = await VideosManager.deleteVideoByVideosId(video_id);
        res.send(result);
    } else {
        if (!videoExists)
            res.status(404).send("Video not found or user is ");
        else if (!userAllowedToDelete) {
            res.status(401).send("User not allowed to delete video");
        } else {
            res.status(404).send("User not allowed to delete and video was not found");
        }
    }
})

/**
 * @swagger
 * /videos/{videoId}/comments/{commentId}:
 *  delete:
 *      security:
 *          - Bearer:
 *              - user
 *              - admin
 *      tags:
 *      - videos
 *      summary: deletes video, it's reactions and comments.
 *      parameters:
 *      -   name: videoId
 *          in: path
 *          description: video's Id.
 *          required: true
 *      -   name: commentId
 *          in: path
 *          description: video's Id.
 *          required: true
 *      -   name: requesterId
 *      responses:
 *          '200':
 *              description: Successful operation
 *          '401':
 *              description: User not allowed to delete this video.
 *          '404':
 *              description: Video not found
 */
router.delete("/:video_id/comments/:comment_id", auth, auth_admin, async (req, res) =>{
    const video_id = parseInt(req.params.video_id);
    const videoExist = await VideosManager.getVideoById(video_id);
    const comment_id = parseInt(req.params.comment_id);
    const commentExists = await CommentManager.getCommentByItsId(comment_id);
    const isAdmin = res.locals.isAdmin;
    if (videoExist && (isAdmin || commentExists)) {
        const result = await CommentManager.deleteCommentById(comment_id);
        res.send(result);
    } else {
        res.status(404).send("Video or comment not found");
    }
})


module.exports = router;
